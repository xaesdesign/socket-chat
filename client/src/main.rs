use std::net::{TcpStream, SocketAddr};
use std::str::FromStr;
use std::time::Duration;
use std::io::{Read, Write, ErrorKind, stdin};
use std::thread;
use std::sync::mpsc::{channel, TryRecvError};

const MESSAGE_SIZE: usize = 64;
const LOCAL_ADDRESS: &str = "127.0.0.1:8000";
const SLEEP_DURATION: Duration = Duration::from_millis(100);

fn main() -> () {

    let mut client = TcpStream::connect_timeout(
        &SocketAddr::from_str(LOCAL_ADDRESS).expect("Invalid Socket Address."),
        Duration::from_secs(3)
    ).expect(&format!("Connection to {} failed", LOCAL_ADDRESS));

    client.set_nonblocking(true).expect("Failed to set TCP client to non-blocking.");

    // Initializing Channels.

    let (sender, receiver) = channel::<String>();

    // Initializing a second thread. This thread will be responsible for:
    //      - Expect a message from main thread using the channel receiver.
    //      - Writing and sending a message to the socket.

    thread::spawn(move || loop {

        let mut buffer = vec![0; MESSAGE_SIZE];

        // Read the content from buffer.

        match client.read_exact(&mut buffer) {
            Ok(_) => {
                let message = buffer.into_iter().take_while(|&s| s != 0).collect::<Vec<u8>>();
                println!("Message received. (Bytes: {:?})", message);
            },
            Err(error) if(error.kind() == ErrorKind::WouldBlock) => (),
            Err(_) => {}
        }

        // Writing a message to the buffer.

        match receiver.try_recv() {
            Ok(message) => {
                let mut buffer = message.clone().into_bytes();
                buffer.resize(MESSAGE_SIZE, 0);
                client.write_all(&buffer).expect("Writing to socket's buffer failed.")
            },
            Err(TryRecvError::Empty) => (),
            Err(TryRecvError::Disconnected) => break
        }

        thread::sleep(SLEEP_DURATION);

    });

    // From the main thread, read the users input and send it to the secondary thread by using the
    // channel's sender.

    loop {
        let mut buffer = String::new();
        stdin().read_line(&mut buffer).expect("Reading line failed");
        if buffer == ":quit" || sender.send(buffer).is_err() { break }
    }

}
