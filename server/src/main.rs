use std::net::{TcpListener, SocketAddr, TcpStream};
use std::sync::mpsc::channel;
use std::thread;
use std::io::{Read, ErrorKind, Write};
use std::time::Duration;
use std::str::FromStr;

const ADDRESS: &str = "127.0.0.1:8000";
const MESSAGE_SIZE: usize = 64;
const SLEEP_DURATION: Duration = Duration::from_millis(100);

fn main() {

    let address = SocketAddr::from_str(ADDRESS).expect("Invalid Socket Address");
    let server = TcpListener::bind(address).expect("Binding address failed.");

    server.set_nonblocking(true).expect("Failed to set server non-blocking");

    let mut clients: Vec<TcpStream> = vec![];
    let (sender, receiver) = channel::<String>();

    loop {

        // Accept clients trying to connect.

        if let Ok(( mut socket, address )) = server.accept() {

            println!("Client ({}) connected.", address);

            // Saving client (TCP Stream) into client vector.

            clients.push(socket.try_clone().expect("Couldn't clone TCP Stream"));

            let sender = sender.clone();

            // When client is accepted, run on a new thread a function that reads the content of the client's
            // socket and prints it to the console.

            thread::spawn(move || loop {

                let mut buffer = [0; MESSAGE_SIZE];

                match socket.read_exact(&mut buffer) {
                    Ok(_) => {
                        let message = buffer.to_vec();
                        let message = String::from_utf8(message).expect("Couldn't convert buffer content to a valid string.");
                        println!("Message received from ({}): {}", address, message);
                        sender.send(message).expect("Failed to send message to main thread.");
                    }
                    Err(ref error) if error.kind() == ErrorKind::WouldBlock => (),
                    Err(_) => {
                        println!("Closing connection with: ({}).", address);
                        break;
                    }
                }

            });

        }

        // Each the a sender on any of the secondary threads send messages to the receiver,
        // let known to the clients saved (TCP Streams) that their message has been received.

        if let Ok(message) = receiver.try_recv() {
            clients = clients.into_iter().filter_map(|mut client| {
                let mut buffer = message.clone().into_bytes();
                buffer.resize(MESSAGE_SIZE, 0);
                client.write_all(&buffer).map(|_| client).ok()
            }).collect::<Vec<_>>();
        }

        thread::sleep(SLEEP_DURATION);

    }



}
